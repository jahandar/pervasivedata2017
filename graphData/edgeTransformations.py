import csv
import requests
import base64
import json
import time

# Open json file to read contents


with open('data/edges.json') as data_file:    
    edges = json.load(data_file)

with open('data/nodes.json') as data_file:    
    nodes = json.load(data_file)

outputdata = []
nodeIndex = {}
nodeIDs = []
noMoreEdges = []
representator = {}


def findRepresentatives(node):
	if representator[node] == node:
		return node
	else:
		# print(type(node))
		# print(type(representator[node]))
		# print(node)
		# print(representator[node])
		return findRepresentatives(representator[node])


i = 0

for i in range(0, len(nodes['nodes'])):
	nodeIndex[nodes['nodes'][i]['id']] = i
	nodeIDs.append(nodes['nodes'][i]['id'])
	representator[i] = i

for i in range(0, len(edges)):
	try:
		sourceNode = edges[i]["source"]
		targetNode = edges[i]["target"]
	except:
		continue
	if sourceNode in nodeIDs and targetNode in nodeIDs:
		sourceID = nodeIndex[edges[i]["source"]];
		targetID = nodeIndex[edges[i]["target"]];
		sourceRepresentator = findRepresentatives(sourceID)
		targetRepresentator = findRepresentatives(targetID)
		if sourceRepresentator != targetRepresentator:
			representator[targetID] = sourceRepresentator
			edges[i]["source"] = sourceID
			edges[i]["target"] = targetID

for edge in edges[:]:
	if int(edge["source"]) > len(nodes['nodes']):
		edges.remove(edge)



outputData = {'nodes':nodes['nodes'], 'links':edges}

dataJson = open('data/fewerEdgesGraphData.json', 'w+')
json.dump(outputData, dataJson)


