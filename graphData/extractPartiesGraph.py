import csv
import requests
import base64
import json
import time

# Open json file to read contents

with open('data/graphData.json') as data_file:
    nodes = json.load(data_file)

outputdata = [{"nodes":[{"id":0, "name":"Labour", "group":2},{"id":1, "name":"Liberal Democrats", "group":3},{"id":2, "name":"SNP", "group":4},{"id":3, "name":"UKIP", "group":5},{"id":4, "name":"Green", "group":6},{"id":5, "name":"Others", "group":7}], "links":[]}, {"nodes":[{"id":0, "name":"Conservatives", "group":1},{"id":1, "name":"Liberal Democrats", "group":3},{"id":2, "name":"SNP", "group":4},{"id":3, "name":"UKIP", "group":5},{"id":4, "name":"Green", "group":6},{"id":5, "name":"Others", "group":7}], "links":[]}, {"nodes":[{"id":0, "name":"Conservatives", "group":1},{"id":1, "name":"Labour", "group":2},{"id":2, "name":"SNP", "group":4},{"id":3, "name":"UKIP", "group":5},{"id":4, "name":"Green", "group":6},{"id":5, "name":"Others", "group":7}], "links":[]}, {"nodes":[{"id":0, "name":"Conservatives", "group":1},{"id":1, "name":"Labour", "group":2},{"id":2, "name":"Liberal Democrats", "group":3},{"id":3, "name":"UKIP", "group":5},{"id":4, "name":"Green", "group":6},{"id":5, "name":"Others", "group":7}], "links":[]}, {"nodes":[{"id":0, "name":"Conservatives", "group":1},{"id":1, "name":"Labour", "group":2},{"id":2, "name":"Liberal Democrats", "group":3},{"id":3, "name":"SNP", "group":4},{"id":4, "name":"Green", "group":6},{"id":5, "name":"Others", "group":7}], "links":[]}, {"nodes":[{"id":0, "name":"Conservatives", "group":1},{"id":1, "name":"Labour", "group":2},{"id":2, "name":"Liberal Democrats", "group":3},{"id":3, "name":"SNP", "group":4},{"id":4, "name":"UKIP", "group":5},{"id":5, "name":"Others", "group":7}], "links":[]}, {"nodes":[{"id":0, "name":"Conservatives", "group":1},{"id":1, "name":"Labour", "group":2},{"id":2, "name":"Liberal Democrats", "group":3},{"id":3, "name":"SNP", "group":4},{"id":4, "name":"UKIP", "group":5},{"id":5, "name":"Green", "group":6}], "links":[]}]
i = 0
nodeIndex = {}	

followMatrix = [[0]*8 for i in range(8)]

for i in range(0, len(nodes['nodes'])):
	gropuID = nodes["nodes"][i]["group"]
	followMatrix[gropuID][0] += 1
	outputdata[gropuID-1]['nodes'].append(nodes["nodes"][i])
	nodeIndex[i] = len(outputdata[gropuID-1]["nodes"]) - 1

for i in range(0, len(nodes["links"])):
	originalSource = nodes["links"][i]["source"]
	originalTarget = nodes["links"][i]["target"]
	sourceGroup = nodes["nodes"][originalSource]["group"]
	targetGroup = nodes["nodes"][originalTarget]["group"]
	followMatrix[sourceGroup][targetGroup] += 1
	currentEdge = nodes["links"][i]
	currentEdge["source"] = nodeIndex[originalSource]
	if sourceGroup == targetGroup:
		currentEdge["target"] = nodeIndex[originalTarget]
	else: 
		currentEdge["target"] = targetGroup - 1
	outputdata[sourceGroup - 1]["links"].append(currentEdge)


dataJson = open('data/followersMatrix.json', 'w+')
json.dump(followMatrix, dataJson)
dataJson = open('data/party1.json', 'w+')
json.dump(outputdata[0], dataJson)
dataJson = open('data/party2.json', 'w+')
json.dump(outputdata[1], dataJson)
dataJson = open('data/party3.json', 'w+')
json.dump(outputdata[2], dataJson)
dataJson = open('data/party4.json', 'w+')
json.dump(outputdata[3], dataJson)
dataJson = open('data/party5.json', 'w+')
json.dump(outputdata[4], dataJson)
dataJson = open('data/party6.json', 'w+')
json.dump(outputdata[5], dataJson)
dataJson = open('data/party7.json', 'w+')
json.dump(outputdata[6], dataJson)


