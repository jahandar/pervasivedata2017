import csv
import requests
import base64
import json
import time


# Extracts node data from candidates.csv




# Open csv file to read contents
csvfile = open('datafiles/candidates.csv', encoding='UTF-8', newline='')

parties = {'party:52': 1, 'party:53': 2, 'party:90': 3, 'party:102': 4, 'party:85': 5, 'party:63': 6}

# Read twitter IDs
nodes = []
candidatesCsv = csv.reader(csvfile)
nextParty = 7
for row in candidatesCsv:
	if bool(row[27]) and row[27].isdigit():
	    if row[7] in parties:
	        group = parties[row[7]]
	    else:
	    	parties[row[7]] = nextParty
	    	group = nextParty
	    	nextParty += 1
	    entry = {'id': row[27], 'group': group, 'username': row[15], 'name': row[1]}
	    nodes.append(entry)
	# Delete first item as it is label
del (nodes[0])

data = {'nodes': nodes, 'links':[]}

dataJson = open('data/nodes.json', 'w+')
json.dump(data, dataJson)


