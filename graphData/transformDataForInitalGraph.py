import csv
import requests
import base64
import json
import time

# Open json file to read contents


with open('data/party6.json') as data_file:    
    nodes = json.load(data_file)


for i in range(0, len(nodes["links"])):
	nodes["links"][i]["source"] = nodes["nodes"][int(nodes["links"][i]["source"])]["id"]
	nodes["links"][i]["target"] = nodes["nodes"][int(nodes["links"][i]["target"])]["id"]



dataJson = open('data/party6_2.json', 'w+')
json.dump(nodes, dataJson)


