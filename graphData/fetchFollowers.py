"""
    Script to fetch followers of users given by userIDs.
    Twitter API limits number of API requests for a time slice, so sending loads of queries is not possible.
    This results in a long running time.
    To overcome this, script uses multiple Twitter Apps to speed up the process. If one app has reached its limits,
     it will switch to another app. If all apps reached their limits, it will wait till one of them becomes available again.
    Loads data from csv file, iterates through twitter user ids and fetches followers' ids for each user.
    Saves its state, so can be paused (interrupted) and resumed later.
    Twitter Apps which script will use must be configured in config.py file.
    :argument: 1st argument is starting point in csv file. If not given, pivot will be loaded from saved state.

    Uses TwitterApp class.
    Saves and loads data from followers.json.
    Structure of followers.json :
    [
        {
            id: <id>,
            followers: [ <follower_id>,...]
        }
    ]

    Saves and loads pivot id from scriptData file.
    Scripts starts from that entry in csv file.

    In some cases, followers of a user is inaccessible ( causes can account doesn't exist anymore, it is private).
    In that case, script ignores that user.

    Sometimes, there happens to be problem with Twitter Apps. App can blacklisted if it exhausts Twitter
     or user can simply delete that app. In that case, script switches to another app.
"""

# !/usr/bin/python3
import csv
import json
import sys
sys.path.insert(1, '../general')
from TwitterApp import TwitterApp

sys.path.insert(0, '..')
from config import twitterApps, candidatesCsv


def load():
    """
    Loads state of the script. Reads from data/followers.json and scriptData
    :return: followersData: A array of user objects with twitterID and list of followers
    :return: pivot: Starting point of the script
    """
    # Read existing data
    followersJsonF = open('data/followers.json', 'r')
    followersData = json.load(followersJsonF)
    followersJsonF.close()

    # Read pivot ID from file
    scriptData = open('scriptData', 'r')
    pivot = 0
    try:
        pivot = int(sys.argv[1])
    except IndexError:
        pivot = int(scriptData.readline())
    scriptData.close()
    return followersData, pivot


def readcsv():
    """
    Opens and reads csv file to determine list of twitter ids which followers will be fetched.
    :return: twitterIDs: List of twitter ids
    """
    # Read twitter IDs
    # Open csv file to read contents
    csvFile = open(candidatesCsv, newline='')
    twitterIDs = []
    candidatesCsv = csv.reader(csvFile)
    # Iterate through IDs
    for row in candidatesCsv:
        twitterID = row[27]
        # Check if twitterID exists and it is a proper ID
        if bool(twitterID) and twitterID.isdigit():
            twitterIDs.append(row[27])

    # Delete first object as it is header
    del (twitterIDs[0])
    return twitterIDs


def main():
    """
    Main function for fetching.
    Loads data and pivot, fetches followers and saves data and pivot.
    :return: none
    """
    # Twitter apps' credentials
    credentials = twitterApps

    # Create TwitterApp object to work with
    apps = []
    for credential in credentials:
        apps.append(TwitterApp(credential['id'], credential['twitterKey'], credential['twitterSecret']))

    twitterIDs = readcsv()

    # Start fetching followers
    while True:
        i = 0
        # Select app
        currentApp = apps[i]
        # Load data and pivot
        followersData, pivot = load()
        # Determine limit
        limit = currentApp.getlimitforfollowers()
        # If limit was reached, switch to another app
        if limit == 0:
            print("Current app with id " + str(currentApp.returnid()) + " has reached to its rate limit")
            currentApp = apps[(i + 1) % len(apps)]
            print("New app is with id " + str(currentApp.returnid()))

        # Get follower IDs
        followers = currentApp.getfollowers(twitterIDs[pivot])

        # If followers is "not_found", it means there is a problem with user(it doesn't exist, account is private, etc.)
        #  and followers can't be fetched. Just pass
        if followers == "not_found":
            # TODO
            pass

        # If followers is "unauthorized", it means there is a problem with app (it is blacklisted, deleted, etc.)
        # and can't be used. Switch to another app
        elif followers == "unauthorized":
            print("Current app with id " + str(currentApp.returnid()) + " is unauthorized")
            currentApp = apps[(i + 1) % len(apps)]
            print("New app is with id " + str(currentApp.returnid()))

        # If there is no problem with fetched data, append them to loaded data, update pivot and save them both
        else:
            followersData.append(followers)
        pivot += 1
        save(followersData, pivot)


def save(followersdata, pivot):
    """
    Saves followersdata and pivot
    :param followersdata: Data to be saved
    :param pivot: Pivot to be saved
    :return:
    """
    # Save data
    followersJsonF = open('data/followers.json', 'w')
    json.dump(followersdata, followersJsonF)
    followersJsonF.close()

    # Save pivot
    print("Next pivot ID will be: " + str(pivot))
    scriptData = open('scriptData', 'w')
    scriptData.write(str(pivot))
    scriptData.close()


if __name__ == '__main__':
    main()
