import csv
import requests
import base64
import json
import time

# Open json file to read contents

def binary_search(array, target):
    lower = 0
    upper = len(array)
    while lower < upper:   
        x = lower + (upper - lower) 
        val = array[x]
        if target == val:
            return x
        elif target > val:
            if lower == x:   
                break        
            lower = x
        elif target < val:
            upper = x

with open('data/sortedFollowers.json') as data_file:    
    data = json.load(data_file)
outputdata = []
i = 0
for l in data:
	i += 1
	currTwitterID = l['id'];
	print(currTwitterID)
	for l2 in data:
		if currTwitterID == l2['id']:
			continue
		if binary_search(l2['followers'], int(currTwitterID)) is not None:
			outputdata.append({"source": currTwitterID, "target": l2['id'], "value" : 1})
	print(i)
	

dataJson = open('data/edges.json', 'w+')
json.dump(outputdata, dataJson)


