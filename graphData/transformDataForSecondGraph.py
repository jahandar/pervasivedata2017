import csv
import requests
import base64
import json
import time

# Open json file to read contents


with open('data/graphData3.json') as data_file:    
    nodes = json.load(data_file)

outputdata = {"nodes":[], "edges":[]}
nodeIndex = {}
colors = ['#0087DC', '#0087DC', '#DC241F', '#FDBB30', '#FFFF00', '#70147A', '#83F52C', '#808080', '#0087DC']
i = 0
for i in range(0, len(nodes['nodes'])):
	outputdata['nodes'].append({"id":i, "label":nodes['nodes'][i]['name'], "color":colors[nodes['nodes'][i]['group']]})
	# outputdata['nodes'][i]['id'] = i
	# outputdata['nodes'][i]['label'] = nodes['nodes'][i]['name']
	# outputdata['nodes'][i]['color'] = colors[nodes['nodes'][i]['group']]

for i in range(0, len(nodes["links"])):
	outputdata["edges"].append({"from":nodes["links"][i]["source"], "to":nodes["links"][i]["target"]})
	# outputdata["edges"][i]["from"] = edges[i]["source"];
	# outputdata["edges"][i]["to"] = edges[i]["target"];

dataJson = open('data/dataForVis2.json', 'w+')
json.dump(outputdata, dataJson)


