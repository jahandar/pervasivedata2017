# Twitter Data Analysis for 2017 General Elections
Jahandar Musayev, Hikmat Hajiyev &copy; University of Manchester, 2017

Principal Investigators: Prof Rachel Gibson (U of Manchester), Dr. Rosalynd Southern (U of Liverpool), Dr. Cristian Vaccari (Royal Holloway, London)

The University of Manchester, Cathie Marsh Institute and the New Political Communication Unit at Royal Holloway will be conducting a study of UK candidates and voters use of Twitter during the 2017 General Election. As part of that project we will be harvesting and beginning preliminary analysis of a large corpus of tweets during June – July. The project would benefit greatly from the programming skills of trained computer scientists who would be able to help us clean and prepare the datasets for analysis. We would also seek to work with them in conducting the preliminary coding and analysis of the tweets which will involve the application of text mining and machine learning techniques as well as sentiment analysis tools. Some assistance with analysis through visualisation of the twitter data through methods like force directed graphing and network maps would also be very interesting to add to the project. Finally we will also require some assistance in terms of the geocoding of tweets to the UK. The main goal of the project will be to measure the political conversation among candidates (elites) and voters during the course of the campaign with a view to understanding how closely or loosely connected this was at both the aggregate or national level and at the local level within particular constituencies. Which parties and candidates proved most in tune with the popular Twitter verse in terms of issue focus and sentiment and did this ultimately matter for their electoral performance? This would offer a unique insight into the extent to which Twitter is proving to be a key medium for election debate, voter representation and ultimately as a determinant of vote choice.


## Documentation
This document is a brief documentation for project and tools created.

### Functionalities
Tools created for the project contains scripts for data import and cleaning, topic extraction and classification as well as scripts for fetching data (e.g fetching Twitter followers of candidates) and extraction of information (e.g extraction image links from tweets, downloading image).
Tools given below are categorized as main tools, additional tools, and configuration file.

### Configuration file
The main configuration file is self-descriptive `config.py` in the root directory. The database, analysis tools, and Twitter Apps can be configured using this file.

### Main Tools
Tools mentioned below are primary tools.

#### Analysis
Scripts are located in `analysis` directory.

A file named `run.py` is provided to run the analysis methods. One only needs to configure and run this file. Methods are included in `analyseTweet.py` or `resultStatistics.py`.
As default, methods will use Naive-Bayes Classifier for classification (though this can be changed in `config.py` file).
Methods are:

- **LDA()** (originally, useLDA()) :  Calling this method uses [Latent Dirichlet Allocation](https://en.wikipedia.org/wiki/Latent_Dirichlet_allocation) for topic extraction. If *classifyTopics* option in `config.py` is set, it will also try to assign extracted topic to one of the topics in database. This method is more recommended than NMF (below). E.g
```python
LDA()
```
- **NMF()** (originally, useNMF()) : Calling this method uses [Non-negative Matrix Factorization](https://en.wikipedia.org/wiki/Non-negative_matrix_factorization) for topic extraction. If *classifyTopics* option in `config.py` is set, it will also try to assign extracted topic to one of the topics in database.
```python
NMF()
```
- **classifyTweets()** : This method classifies tweets into topics in database. If a list of tweet IDs are given as argument, only those tweets will be classified. Otherwise, every tweet in database will be classified and printed. If database is large, this will give huge amounts of output.
```python
classifyTweets()
```

> ***For first 2 methods below, if "saveResults" option is set in `config.py` file (which is as default), results will be saved in database collection called "results" as well as being printed on screen.***

- **groupClassifyTweetsByUsername()** : This method groups all tweets by users and classifies them. If a list of twitter usernames are given as argument, only those users' tweets will be grouped and classified.
```python
groupClassifyTweetsByUsername(usernames=["1tomcorbin", "aalancraig"])
```
Otherwise, it will process every user's tweets.
```python
groupClassifyTweetsByUsername()
```

- **groupClassifyTweetsByParty()** : This method groups all tweets by parties and clasifies them. If a list of party IDs are given as argument, only those parties' tweets will be grouped and classified.
```python
groupClassifyTweetsByParty(partyIDs=["party:52","party:85"])
```
Otherwise, it will process every party's tweets.
```python
groupClassifyTweetsByParty()
```

- **calcStatisticsForUserGroupTweets()** : This methods takes results of "groupClassifyTweetsByUsername()" and calculates statistics for parties and topics which their candidates tweeted. An id of result in database collection 'results' must be given as argument.
```python
calcStatisticsForUserGroupTweets("597e3a75b7c6667ab9ae456b")
```
Output:
```
{'Alliance - Alliance Party of Northern Ireland': {'Alliance Party': 10,
                                                   'Education': 1,
                                                   'Vote': 5},
 'Animal Welfare Party': {'Vote': 1},
 'British National Party': {'British National Party': 4},
 "Christian Party Proclaiming Christ's Lordship": {'Ulster Unionist Party': 1},
 'Christian Peoples Alliance': {'Coalition': 1,
                                'Jonathan Bartley': 1,
                                'Liberal Democrat Party': 1,
                                'Manifesto': 2,
                                'NHS': 1,
                                'Vote': 1,
                                'not categorized': 2},
 'Church of the Militant Elvis': {'UK General Election': 1},
 'Citizens Independent Social Thought Alliance': {'UK General Election': 1},
 'Common Good': {'Europe': 1},
 'Compass Party': {'Zero Hours': 1},

 ...
```
- **calcStatisticsForPartyGroupTweets()** : This methods takes results of "groupClassifyTweetsByParty()" and calculates statistics for parties and topics which their candidates tweeted. Difference between the one above and this one is, the one above calculates statistics using results of each individiual candidate whereas this one calculates on whole party. An id of result in database collection 'results' must be given as argument.
```python
calcStatisticsForPartyGroupTweets("59812af5b7c66655d6b4bb77")
```
Output:
```
{'Conservative and Unionist Party': {'Conservative Party': 1}

...
```

#### Keyword import and extension

For classification of tweets, model needs training data. In this project, training data is dictionary. Features are keywords and labels are topics. For this purpose, a script called `datadict.py` can be used to import and extend keywords in database. Keywords must be given in `datadict.cvs` (one is provided). The script is pretty self-explanatory. One only needs to run `datadict.py` and follow the instructions. He can choose to import data from CSV file or extend dictionary by finding keywords online using existing keywords or topics (extension will be done using topic names only or topic names and 10 keywords in each topic only. 10 keywords limit is set to prevent over-extension of dictionary and collection of unrelated words.)

#### Image collection
Scripts mentioned below is used for collecting images. As the number of tweets is large, a script for downloading them is provided instead of giving images altogether.

- `getImagesFromMongoDB.py` : When it is run, this script will extract image links from tweet text for each tweet in database and save them alongside tweet document in database in field called 'images'. As the number of tweets is very large, this script takes a lot of time, but can be paused (interrupted) and resumed later. In each run, it will pick from where it has left and continue.
- `downloadImages.py` : When it is run, it will start to download images using URLs in database. It will place images in a folder called 'img' in the same directory. For each candidate, it will create a sub-directory and download images to that folder. Also, it will put a small text file containing basic information about candidate in each subdirectory.
