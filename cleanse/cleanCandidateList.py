#!/usr/bin/python3
import csv
import psycopg2
import sys
import json
import datetime
import pytz
import requests
from pymongo import MongoClient

sys.path.insert(0, '../general')

from TwitterApp import TwitterApp

sys.path.insert(0, '..')
from config import mongoDbURI, topicModelling, mongodbName

# Configure database client
client = MongoClient(host=mongoDbURI())
db = client[mongodbName]

twitterapp = TwitterApp(1, "WXbwVHs8wvQMurJjGH4Q2fuaC", "uLhxvcdoPDVX2TUS6YZdp3hSZMlNtnjYmOP6kKY7fn6p38P4MZ")


def updateIDs():
    """
    Fetches user names from candidates collection and updates Twitter User IDs for them
    :return:
    """
    userIDs = list(db["cleanedCandidateData"].distinct("twitter_username"))
    chunks = []
    for i in range(0, len(userIDs), 100):
        chunks.append(userIDs[i:i + 100])

    for chunk in chunks:
        info = twitterapp.lookup(chunk, "name")
        for entry in info:
            db["cleanedCandidateData"].update_one({"twitter_username": entry['screen_name']},
                                                  {"$set": {"twitter_user_id": entry['id']}})


def updateUsernames():
    """
    Fetches Twitter User IDs from candidates collection
    If username has changed, it add it to database
    :return:
    """
    userIDs = list(db["cleanedCandidateData"].distinct("twitter_user_id"))
    chunks = []
    for i in range(0, len(userIDs), 100):
        chunks.append(userIDs[i:i + 100])

    for chunk in chunks:
        info = twitterapp.lookup(chunk, "id")
        for entry in info:
            db["cleanedCandidateData"].update_one({"twitter_user_id": entry['id']},
                                                  {"$addToSet": {"twitter_username": entry['screen_name']}})


def updateUserIDsFromDatabase():
    """
    Fetches Twitter Usernames from tweets collection and checks for IDs
    :return:
    """
    usernames = list(db["tweets"].distinct("user_screen_name"))
    chunks = []
    for i in range(0, len(usernames), 100):
        chunks.append(usernames[i:i + 100])

    for chunk in chunks:
        info = twitterapp.lookup(chunk, "name")
        for entry in info:
            db["cleanedCandidateData"].update_one({"twitter_username": entry['screen_name']},
                                                  {"$set": {"twitter_user_id": entry['id']}})


def checkStrayTweetOwners():
    """
    Fetches user names from stray (owner is unknown) tweets and checks if they are really valid.
     If they are, prints user name
    :return:
    """
    owners = list(db['tweets'].aggregate([
        {'$match': {'twitter_user_id': {'$exists': False}}},
        {'$group': {'_id': "$user_screen_name", 'count': {'$sum': 1}}}]))

    for owner in owners:
        try:
            response = requests.get("https://twitter.com/" + owner['_id'])
            if response.status_code == 200:
                print(owner["_id"] + " is active in Twitter")
        except TypeError:
            pass


def placeUserIDs():
    """
    Adds Twitter User ID to tweets for known candidates
    :return:
    """
    users = list(db["cleanedCandidateData"].find({}, {"_id": 0, "twitter_username": 1, "twitter_user_id": 1}))

    for user in users:
        try:
            for username in user["twitter_username"]:
                print("Updating tweets for " + username)
                db["tweets"].update_many(
                    {"$and": [{"user_screen_name": username}, {"twitter_user_id": {"$exists": False}}]},
                    {"$set": {"twitter_user_id": user["twitter_user_id"]}})
        except KeyError:
            pass


def convertTweetDateTime():
    """
    Converts Twitter date and time into a MongoDB-compatible date and time
    :return:
    """

    print("Converting all tweets from Twitter date-time format to MongoDB date-time format")
    n = 0
    totalCount = db["tweets"].find().count()
    for doc in db["tweets"].find():
        n += 1
        if n % 10000 == 0:
            print(str((n / totalCount) * 100) + "%\r")
        ts = datetime.datetime.strptime(doc["tweet_created_at"], '%a %b %d %H:%M:%S +0000 %Y').replace(tzinfo=pytz.UTC)
        db["tweets"].update_one({"_id": doc["_id"]}, {"$set": {"tweet_created_at": ts}})


if __name__ == "__main__":
    # updateIDs()
    # updateUsernames()
    # updateUserIDsFromDatabase()
    # placeUserIDs()    # Takes a while, be careful
    # checkStrayTweetOwners()
    # convertTweetDateTime()    # Can be used only once
    client.close()
