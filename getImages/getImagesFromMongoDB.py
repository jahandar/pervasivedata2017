"""
    Script to extract images from tweets
"""

import random
import time
import datetime
import re
import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient

client = MongoClient()
db = client["sp"]
collection = db['tweets']
finalData = []
numberOfDocs = collection.find({"images": {"$exists": False}}).count()
visited = 0

print("Fetching images for %d tweets" % numberOfDocs)

for row in collection.find({"images": {"$exists": False}}):
    t0 = time.time()
    urls = []
    tweetText = str(row["tweet_text"])
    for url in re.findall(
            "(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})",
            tweetText):
        if bool(url):
            try:
                urlData = requests.get(url)
                if urlData.status_code == 200:
                    data = urlData.text
                    bs = BeautifulSoup(data, "html.parser")
                    imgUrl = bs.select('img[data-aria-label-part]')
                    if imgUrl is not None and bool(imgUrl):
                        urls.append(imgUrl[0]['src'])
                # time.sleep(0.1)
            except:
                pass
    collection.update_one({"_id": row["_id"]}, {'$set': {"images": urls}})
    visited += 1
    print("", end="\r")
    print("Progress: %2.3f" % ((visited / numberOfDocs) * 100),
          end="\r")

print("Progress: %2.3f %%" % ((visited / numberOfDocs) * 100))
print("Fetching is done")
