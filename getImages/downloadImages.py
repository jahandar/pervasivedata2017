import requests
import os
import sys
import time
import random
from pymongo import MongoClient

sys.path.insert(0, '..')
from config import mongoDbURI, topicModelling, mongodbName

# Configure database client
client = MongoClient(host=mongoDbURI())
db = client[mongodbName]


def download():
    images = "img"
    print("Downloading images from database")
    for candidate in db["cleanedCandidateData"].find({"twitter_user_id": 1235025320}).limit(20):
        print("> Downloading images for " + candidate["twitter_username"][-1])
        count = 0
        exist = 0
        directory = os.path.join(images, candidate["twitter_username"][-1])
        if not os.path.exists(directory):
            os.makedirs(directory)
        for tweet in db["tweets"].find({"twitter_user_id": candidate["twitter_user_id"], "images": {"$exists": True}}):
            tweet_name = str(tweet["_id"]) + ".jpg"
            for url in tweet["images"]:
                filename = os.path.join(directory, tweet_name)
                if not os.path.exists(filename):
                    r = requests.get(url)
                    with open(filename, "wb") as image_file:
                        image_file.write(r.content)
                    count += 1
                    time.sleep((random.randrange(0, 1))/2)
                else:
                    exist += 1
        print("  -> %d images are downloaded" % count)
        if exist != 0:
            print("  -> %d images were existed already" % exist)


if __name__ == "__main__":
    download()
