#!/usr/bin/python3

import requests
import base64
import time
import random


class TwitterApp:
    """
        Class for Twitter API manipulation.
        Contains general-use methods to fetch followers, lookup for information on a list of twitter users.
        Also, contains methods to renew bearer token, determine limit left for app
        :param: appid: Number to identify instance of class (not related to Twitter App)

        Credentials
        :param twitter_key: Twitter App Key to be used
        :param twitter_secret: Twitter App Secret to be used
    """

    def __init__(self, appid, twitter_key, twitter_secret):
        self.id = appid

        # Set credentials
        self.twitterKey = twitter_key
        self.twitterSecret = twitter_secret
        self.bearerToken = ""
        self.setbearertoken()

    def returnid(self):
        """
        Returns id of app
        :return: self.id
        """
        return self.id

    def setbearertoken(self):
        """
        Sets new (probably same as old token as twitter preserves same token a time) bearer token for Twitter App.
        Bearer token is essential for Twitter OAuth 2.0 API
        :return: none
        """
        bearerToken = self.twitterKey + ":" + self.twitterSecret
        bearerToken64 = base64.b64encode(bearerToken.encode('utf-8'))

        # Url of request
        tokenUrl = "https://api.twitter.com/oauth2/token"

        # Set header for request
        headersForBearer = {"User-Agent": "My Twitter App v1.0.23",
                            "Authorization": "Basic " + bearerToken64.decode('utf-8'),
                            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8", "Content-Length": "29",
                            "Accept-Encoding": "gzip"}

        # Send request
        tokenResponse = requests.post(tokenUrl, data="grant_type=client_credentials", headers=headersForBearer).json()
        print("New bearer token obtained")

        # Set new token
        self.bearerToken = tokenResponse['access_token']

    def getlimitforfollowers(self):
        """
        Returns limit for current app to fetch followers
        :return: limit: Limit for request. If it is 0 that means limit is reached.
        """
        # Header for request
        headersForFollowers = {"User-Agent": "My Twitter App v1.0.23",
                               "Authorization": "Bearer " + self.bearerToken,
                               "Accept-Encoding": "gzip"}

        # Url of request
        rateLimitFollowersUrl = 'https://api.twitter.com/1.1/application/rate_limit_status.json?resources=followers'

        # Send request
        rateLimitFollowersResponse = requests.get(rateLimitFollowersUrl, headers=headersForFollowers).json()

        # Parse and return limit
        return rateLimitFollowersResponse['resources']['followers']['/followers/ids']['remaining']

    def getfollowers(self, twitterID):
        """
        Fetches the followers of given Twitted ID
        :param twitterID: Twitter ID of user to be fetched
        :return: Dictionary consisted of user ID and list of follower IDs
        """

        # Set up cursor in case multi-page response
        cursor = -1
        followers = []
        waitingCount = 0

        # Check for bearer token
        self.setbearertoken()
        while cursor != 0:

            # Header for request
            headersForFollowers = {"User-Agent": "My Twitter App v1.0.23",
                                   "Authorization": "Bearer " + self.bearerToken,
                                   "Accept-Encoding": "gzip"}

            # Check for limit
            limit = self.getlimitforfollowers()

            # If limit is not reached
            if limit != 0:
                self.getlimitforfollowers()
                print("-----------------------------------------")
                print("Fetching followers for " + str(twitterID))
                # Send request
                # Check response status code if everything okay
                # If okay, parse response and get list of followers
                # Else, return not_found or unauthorized
                # After each request, sleep for a random period
                followersUrl = "https://api.twitter.com/1.1/followers/ids.json?user_id=" + str(
                    twitterID) + "&cursor=" + str(
                    cursor)
                followersResponse = requests.get(followersUrl, headers=headersForFollowers)
                print("Request Result: " + str(followersResponse.status_code))
                if followersResponse.status_code == requests.codes.ok:
                    followers += followersResponse.json()['ids']
                    print("Fetched followers for " + str(twitterID))
                    cursor = followersResponse.json()['next_cursor']
                elif followersResponse.status_code == requests.codes.unauthorized:
                    print(followersResponse.text)
                    return "unauthorized"
                elif followersResponse.status_code == requests.codes.not_found:
                    return "not_found"
                waitingCount = 0
                time.sleep(random.randrange(1, 20))

            # If limit is reached, check again with delays of 1 minute
            else:
                waitingCount += 1
                print("Reached rate limit. Waiting..." + str(waitingCount))
                time.sleep(60)

        print("Follower count for " + str(twitterID) + ": " + str(len(followers)))
        return {'id': twitterID, 'followers': followers}

    def lookup(self, names, criteria):
        """
        Returns user objects from Twitter API
        :param names: List of user ids to lookup
        :param criteria: Specifies to lookup either "names" or "ids"
        :return: list of user objects
        """

        # Set header for request
        headersForLookup = {"User-Agent": "My Twitter App v1.0.23",
                            "Authorization": "Bearer " + self.bearerToken,
                            "Accept-Encoding": "gzip"}

        # Prepare parameters and url
        names = ",".join(str(name) for name in names)
        lookupUrl = "https://api.twitter.com/1.1/users/lookup.json"
        if criteria == "name":
            dataToSend = {"screen_name": names}
        elif criteria == "id":
            dataToSend = {"user_id": names}
        else:
            print("Invalid parameter")
            exit()

        # Send request and return response
        list = requests.post(lookupUrl, data=dataToSend, headers=headersForLookup).json()
        return list
