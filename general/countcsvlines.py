#!/usr/bin/python3
import csv
from os import listdir
from os.path import isfile, join

def calc_numberof_tweets(folder):
    number = 0
    csvfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
    for csvfile in csvfiles:
        csvFile = open(join(folder, csvfile), newline='')
        candidatesCsv = csv.reader(csvFile)
        number += sum(1 for currentRow in candidatesCsv)
    return number