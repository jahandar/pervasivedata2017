"""
    General configurations file
"""

import urllib.parse
import datetime

"""
    MongoDB Connection Credentials
    If there is no authentication, leave username and password empty
"""
mongodbName = "sp"
mongodbCred = {"host": "localhost",
               "port": "",
               "username": "",
               "password": "",
               "authMechanism": "SCRAM-SHA-1",
}

topicModelling = {
    "saveResults": True,

    "nFeatures": 0,  # Set 0 to use all available features (recommended)

    "nSamples": 50,  # Set 0 to use all tweets

    "nTopWords": 0,
    # Must be around average number of keywords in data dictionary. Set 0 to calculate automatically (recommended)

    "nTopics": 5,  # Set 0 to use all topics in database (recommended)

    "displayKeywords": False,  # Set to display top words for each topic

    "classifyTopics": True,  # Set to name topics

    # Values - mnb (Multinomial Naive-Bayes Classifier),
    # svc (Support Vector Classifier)
    # vote (Use all classifiers and vote for result)
    "classifier": "mnb",

    "tweetsCollection": "tweets",  # Select which collection to use

    "candidatesCollection": "cleanedCandidateData",  # Select which collection to use for candidates

    "surveyData": "pcon_wordcount",  # Select which collection to use for topics from survey

    "topic_dict": "survey_topics",

    "dateRange": True,  # If set to True, use range below. Otherwise, they will be ignored

    # datetime(year, month, day[, hour[, minute[, second[, microsecond[,tzinfo]]]]]
    "startDate": datetime.datetime(2017, 5, 11),
    "endDate": datetime.datetime(2017, 6, 8),
}

"""
    List of Twitter Apps that can be used
"""
twitterApps = [
    {'id': 1, 'twitterKey': 'WXbwVHs8wvQMurJjGH4Q2fuaC',
     'twitterSecret': 'uLhxvcdoPDVX2TUS6YZdp3hSZMlNtnjYmOP6kKY7fn6p38P4MZ'},
    {'id': 2, 'twitterKey': '5Tiyjpb6smLhA5vl9lPgdFQB0',
     'twitterSecret': 'WohP4PQ7vIJRJoPYnzurHGAgOhIQxoN2up14rKCN8KfhhlTQAD'},
    {'id': 3, 'twitterKey': 'U7RuWABRUMDidPzIjtlXL1avN',
     'twitterSecret': 'vMDn4qcwVjqLcOXdcIP1Uhi9SAvFe0mzn4yhvdzhE2jtNhqQ2O'}
]

# CSV File containing candidates' information
candidatesCsv = 'datafiles/candidates.csv'


def mongoDbURI():
    """
    Forms a connection URI to pass to MongoClient
    :return: URI to database
    """
    username = urllib.parse.quote_plus(mongodbCred['username'])
    password = urllib.parse.quote_plus(mongodbCred['password'])
    host = mongodbCred['host']
    mechanism = mongodbCred['authMechanism']
    if not bool(username):
        uri = "mongodb://{0}/the_database?authMechanism={1}".format(host, mechanism)
    else:
        uri = "mongodb://{0}:{1}@{2}/the_database?authMechanism={3}".format(username, password, host, mechanism)
    return uri


