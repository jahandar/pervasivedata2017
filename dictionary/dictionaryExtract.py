import csv
import requests
import base64
import json
import time
import sys

# Open csv file to read contents
csvfile = open(sys.argv[1], newline='')

# Read twitter IDs
data = []
candidatesCsv = csv.reader(csvfile)
for row in candidatesCsv:
    entry = {}
    keywords = []
    key = row[0]
    entry['topic'] = key
    for i in range(1, 7):
        if bool(row[i]):
            keywords.append(row[i])
    entry['keywords'] = keywords
    data.append(entry)
dataJson = open(sys.argv[1] + '.json', 'w+')
json.dump(data, dataJson)
