import datetime

__author__ = 'Jahandar Musayev'

from time import time
import sys

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, TfidfTransformer
from sklearn.decomposition import LatentDirichletAllocation, NMF
from pymongo import MongoClient
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import VotingClassifier
from sklearn import svm
import numpy
import pprint
import ast
from scipy.spatial import distance
import bson

from Analyser import Analyser

sys.path.insert(0, '..')
from config import mongoDbURI, topicModelling, mongodbName

# Configure database client
client = MongoClient(host=mongoDbURI())
db = client[mongodbName]

# Import configurations
NUMBER_OF_FEATURES = topicModelling["nFeatures"]
if NUMBER_OF_FEATURES == 0:
    NUMBER_OF_FEATURES = None
NUMBER_OF_SAMPLES = topicModelling["nSamples"]
NUMBER_OF_TOP_WORDS = topicModelling["nTopWords"]
NUMBER_OF_TOPICS = topicModelling["nTopics"]
DISPLAY_KEYWORDS = topicModelling["displayKeywords"]
CLASSIFY_TOPICS = topicModelling["classifyTopics"]
CLASSIFIER = topicModelling["classifier"]
TWEETS_COLLECTION = topicModelling["tweetsCollection"]
CANDIDATES_COLLECTION = topicModelling["candidatesCollection"]
DATE_RANGE = topicModelling["dateRange"]
START_DATE = topicModelling["startDate"]
END_DATE = topicModelling["endDate"]
SAVE_RESULTS = topicModelling["saveResults"]
SURVEY_DATA = topicModelling["surveyData"]
TOPIC_DICT = topicModelling["topic_dict"]

"""
    Different naming convention are used as a result of development over time
"""


def use_LDA(user):
    """
    Groups tweets by user name and classifies
    :param usernames: Array of username strings classify. If None, will use all usernames in database
    :return: topics extracted from tweets using LDA
    """

    analyser = Analyser()

    print("\textracting topics from tweets", end='')
    t0 = time()

    # For each user, select tweets that belong to that user
    # Concatenate them all together to create sample (they will be processed on classification) for each user

    dataset = []
    if DATE_RANGE:
        allTweets = db[TWEETS_COLLECTION].find({'twitter_user_id': user['twitter_user_id'],
                                                'tweet_created_at': {'$gte': START_DATE,
                                                                     '$lt': END_DATE}})
    else:
        allTweets = db[TWEETS_COLLECTION].find({'twitter_user_id': user['twitter_user_id']})

    if allTweets.count() < 1:
        return {}

    for tweet in allTweets:
        dataset.append(tweet['tweet_text'])

    text = open("text.txt", "r").readlines()
    for line in text:
        dataset += line.split(".")

    number_of_topics_in_db = db[TOPIC_DICT].find({}).count()

    topic_ids = None
    names = None
    probabilities = None
    nOfExtractedTopics = 0

    number_of_topics = NUMBER_OF_TOPICS
    while nOfExtractedTopics < 5 and number_of_topics < number_of_topics_in_db:
        # Extract topics
        lda, featureNames = analyser.LDA(dataset, number_of_topics)

        if lda == -1:
            return {}

        topic_ids, names, probabilities = analyser.nameTopics(lda, featureNames)
        nOfExtractedTopics = numpy.unique(names).size
        number_of_topics += 1

    topTopics = {}
    only_five_topics = 0
    for topic_id, category, probability in zip(topic_ids, names, probabilities):
        if category not in topTopics and only_five_topics < 5:
            if numpy.unique(probability).size != 1:
                topTopics[category] = topic_id
                only_five_topics += 1

    print("...done in %0.3fs." % (time() - t0))
    return topTopics


def get_users(usernames=None, limit=0):
    """
    Return data for given usernames or return data for all users in database
    :param usernames: Users to be fetched from database
    """
    print("Fetching users form database", end='')
    t0 = time()

    if usernames:
        users = db[CANDIDATES_COLLECTION].find({"twitter_username": {"$in": usernames}},
                                               {"id": 1, "twitter_user_id": 1, "twitter_username": 1,
                                                "party_name": 1, "party_id": 1,
                                                "name": 1, "post_label": 1})
    else:
        # users = db[CANDIDATES_COLLECTION].find({"twitter_username": {"$exists": True}},
        #                                        {"twitter_user_id": 1, "twitter_username": 1,
        #                                         "party_name": 1, "party_id": 1,
        #                                         "name": 1, "post_label": 1}).limit(limit)

        users = db[CANDIDATES_COLLECTION].find({},
                                               {"id": 1, "twitter_user_id": 1, "twitter_username": 1,
                                                "party_name": 1, "party_id": 1,
                                                "name": 1, "post_label": 1}).limit(limit)

    print("...done in %0.3fs." % (time() - t0))
    return users


def calc_weekly_count_tweets(user):
    """
    Get number of tweets each week in a date range
    :param user: User object
    """
    print('\tcalculating number of tweets by weeks', end='')
    t0 = time()

    weeks = []
    startOfWeek = START_DATE
    endOfWeek = startOfWeek + datetime.timedelta(days=7)
    week = 1
    while endOfWeek <= END_DATE:
        pipeline = [
            {'$match': {'$and': [{'tweet_created_at': {'$gte': startOfWeek,
                                                       '$lt': endOfWeek}},
                                 {'twitter_user_id': user['twitter_user_id']}]}
             },

            {'$group': {
                '_id': week,
                'count': {'$sum': 1}}
            }
        ]

        tweets = list(db[TWEETS_COLLECTION].aggregate(pipeline))
        if not tweets:
            tweets = [{'_id': week, 'count': 0}]

        weeks += tweets
        startOfWeek = endOfWeek
        endOfWeek = startOfWeek + datetime.timedelta(days=7)
        week += 1
    print("...done in %0.3fs." % (time() - t0))
    return weeks


def calc_mean_and_SD(weeks):
    """
    Calculate and return mean and standard deviation from the weekly count of tweets
    :param weeks: Array of weekly count of tweets
    """
    print('\tcalculating standard deviation', end='')
    t0 = time()

    weekly_numbers = [week['count'] for week in weeks]
    mean = numpy.mean(weekly_numbers)
    SDeviation = numpy.std(weekly_numbers)

    print("...done in %0.3fs." % (time() - t0))
    return mean, SDeviation


def calc_diff_between_weeks(weeks, first, last):
    """
    Calculate difference between number of tweets in given weeks (last - first)
    :param weeks: Array of weekly count of tweets
    :param first: Subtrahend
    :param last: Minuend
    """
    print('\tcalculating difference between week ' + str(first) + ' and ' + str(last), end='')
    t0 = time()

    firstWeek = None
    lastWeek = None
    for week in weeks:
        if week['_id'] == first:
            firstWeek = week['count']
        if week['_id'] == last:
            lastWeek = week['count']

    if firstWeek is None or lastWeek is None:
        print("Problem with weeks")
        exit()
    diff = lastWeek - firstWeek
    print("...done in %0.3fs." % (time() - t0))
    return diff


def calc_ratio_of_weeks(weeks, first, last):
    """
    Calculate ratio of number of tweets in given weeks (last / first)
    :param weeks: Array of weekly count of tweets
    :param first: Divisor
    :param last: Dividend
    """
    print('\tcalculating ratio of week ' + str(first) + ' and ' + str(last), end='')
    t0 = time()

    firstWeek = None
    lastWeek = None
    for week in weeks:
        if week['_id'] == first:
            firstWeek = week['count']
        if week['_id'] == last:
            lastWeek = week['count']

    if firstWeek is None or lastWeek is None:
        print("\nProblem with weeks")
        exit()

    if firstWeek == 0:
        ratio = -1
    else:
        ratio = lastWeek / firstWeek
    print("...done in %0.3fs." % (time() - t0))

    return ratio


def calc_slope(user):
    """
    Calculate slope of a user's activity
    :param user: User
    """
    print('\tcalculating slope', end='')
    t0 = time()

    pipeline = [
        {'$match': {'$and': [{'tweet_created_at': {'$gte': START_DATE,
                                                   '$lt': END_DATE}},
                             {'twitter_user_id': user['twitter_user_id']}]}
         },

        {'$project': {
            'year': {'$year': '$tweet_created_at'},
            'month': {'$month': '$tweet_created_at'},
            'day': {'$dayOfMonth': '$tweet_created_at'}}
        },

        {'$group': {
            '_id': {'year': '$year', 'month': '$month', 'day': '$day'},
            'count': {'$sum': 1}}
        }
    ]

    tweets = list(db[TWEETS_COLLECTION].aggregate(pipeline))

    overallSlope = 0
    for day in range(1, len(tweets)):
        overallSlope += tweets[day]['count'] - tweets[day - 1]['count']

    print("...done in %0.3fs." % (time() - t0))
    return overallSlope


def calc_tweet_count(user):
    """
    Calculate overall number of tweets by user in a period
    :param user: User
    """
    print('\tcalculating overall number of tweets over a period', end='')
    t0 = time()

    count = db[TWEETS_COLLECTION].find({'twitter_user_id': user['twitter_user_id'],
                                        'tweet_created_at': {'$gte': START_DATE,
                                                             '$lt': END_DATE}}).count()
    print("...done in %0.3fs." % (time() - t0))
    return count


def calc_all_tweet_count(user):
    """
    Calculate overall number of tweets by user
    :param user: User
    """
    print('\tcalculating overall number of tweets', end='')
    t0 = time()

    count = db[TWEETS_COLLECTION].find({'twitter_user_id': user['twitter_user_id']}).count()
    print("...done in %0.3fs." % (time() - t0))
    return count


# def calc_follower_number_last_week(user):
#     """
#     Calculate number of followers at the last week of campaign
#     :param user: User
#     """
#     print('\tcalculating number of followers in last week', end='')
#     t0 = time()
#     startOfWeek = END_DATE - datetime.timedelta(days=7)
#     followerCount = db[TWEETS_COLLECTION].find_one({'twitter_user_id': user['twitter_user_id'],
#                                                     'tweet_created_at': {'$gte': startOfWeek,
#                                                                          '$lt': END_DATE}},
#                                                    {'user_followers_count': 1})
#     if followerCount is not None:
#         followerCount = followerCount['user_followers_count']
#     else:
#         followerCount = "unknown"
#     print("...done in %0.3fs." % (time() - t0))
#     return followerCount


def prepare_vectors(topicsFromDB, topicData):
    """
    Prepare vectors for calculating congruence (cosine similarity)
    :param topicsFromDB: Topics in database
    :param topicData: Topics to be put in vector space
    """
    index = {}
    for topic in topicsFromDB:
        index[topic['topic'].lower().strip()] = (0, 0,)

    i = 1
    for topic in topicData:
        topicName = topic['topic']
        if topicName in index:
            index[topicName] = (1, i)
            i += 1

    topicsToSort = []
    for key, value in index.items():
        topicsToSort.append({'topic': key, 'data': value})

    topicsSorted = sorted(topicsToSort, key=lambda k: k['topic'])

    vector = []
    for topic in topicsSorted:
        vector.append(topic['data'][0])
        vector.append(topic['data'][1])

    return vector


def calc_congruence(topicsFromTweets, topicsFromSurvey):
    """
    Calculate congruence (cosine similarity) of topics extracted from tweets and topics obtained from BES survey
    :param topicsFromTweets: Topics extracted from tweets
    :param topicsFromSurvey: Topics obtained by survey
    """
    t0 = time()
    print("\tcalculating congruence", end='')
    topicsFromDB = list(db[TOPIC_DICT].find({}, {'topic': 1, '_id': 0}))
    topicVectorTweet = prepare_vectors(topicsFromDB, topicsFromTweets)
    topicVectorSurvey = prepare_vectors(topicsFromDB, topicsFromSurvey)
    print("...done in %0.3fs." % (time() - t0))
    return distance.cosine(topicVectorTweet, topicVectorSurvey)


def congruence(user):
    """
    Find the congruence for a user
    :param user: User
    """
    t0 = time()
    topicsFromTweets = use_LDA(user)
    converted = []
    for key, value in topicsFromTweets.items():
        topic = {"topic": key.strip().lower(), "ranking/score": value}
        converted.append(topic)

    topicsFromTweets = sorted(converted, key=lambda k: k['ranking/score'])

    print("\tgetting topics from survey", end='')
    topicsFromSurvey = db[SURVEY_DATA].find_one({'Constituency': user['post_label']})
    if topicsFromSurvey is None:
        return topicsFromTweets, [], 1.0
    topicsFromSurvey = ast.literal_eval(topicsFromSurvey['topn'])
    converted = []
    for eachTopic in topicsFromSurvey:
        topic = {"topic": eachTopic[0].strip().lower(), "ranking/score": eachTopic[1]}
        converted.append(topic)

    topicsFromSurvey = sorted(converted, key=lambda k: k['ranking/score'], reverse=True)
    print("...done in %0.3fs." % (time() - t0))

    cosineSimilarity = calc_congruence(topicsFromTweets, topicsFromSurvey)

    return topicsFromTweets, topicsFromSurvey, cosineSimilarity


def create_results(type):
    return db["results"].insert_one({"type": type, "datetime": datetime.datetime.now(), "results": []}).inserted_id


def save_results(results_id, result):
    db["results"].update_one({"_id": results_id}, {"$push": {"results": result}})


def build_variables(usernames=None, create_result=False):
    """
    One of main function. Analyses and builds as much data as possible for a user
    :param usernames: List of user names to be analysed. If None, use all users from data
    """

    if create_result:
        db["results"].delete_many({"type": "Variables"})
        create_results("Variables")

    results_id = db["results"].find_one({"type": "Variables"})["_id"]
    if results_id is None:
        print("No results collection")
        exit()

    results = db["results"].find_one({"type": "Variables"})["results"]
    processed_users = []

    for result in results:
        processed_users.append(result["id"])

    processed_user_count = len(processed_users)

    users = get_users(usernames)

    for user in users:
        t0 = time()
        if user["id"] not in processed_users:
            print("{} - Processing and extracting variables for: {} ( {} ) ".format(processed_user_count,
                                                                                    user['name'],
                                                                                    user['id']))
            try:
                slope = calc_slope(user)
                tweet_count = calc_tweet_count(user)
                all_tweet_count = calc_all_tweet_count(user)

                if all_tweet_count == 0:
                    twitter = 1
                elif tweet_count == 0:
                    twitter = 2
                else:
                    twitter = 3

                # follower_count = calc_follower_number_last_week(user)

                weekly_count = calc_weekly_count_tweets(user)
                nOfTweetsLastWeek = weekly_count[-1]['count']
                mean, standard_deviation = calc_mean_and_SD(weekly_count)
                ratio = calc_ratio_of_weeks(weekly_count, 1, 4)
                difference = calc_diff_between_weeks(weekly_count, 1, 4)

                topicsFromTweets, topicsFromSurvey, cosine_similarity = congruence(user)

                result = {"id": user['id'],
                          "twitter_username": user['twitter_username'],
                          "twitter_user_id": user['twitter_user_id'],
                          "twitter": twitter,
                          "name": user['name'],
                          "slope": slope,
                          "tweet_count": tweet_count,
                          # "follower_count": follower_count,
                          "mean": mean,
                          "standard_deviation": standard_deviation,
                          "ratio": ratio,
                          "difference": difference,
                          "tweets_last_week": nOfTweetsLastWeek,
                          "topicsFromTweets": topicsFromTweets,
                          "topicsFromSurvey": topicsFromSurvey,
                          "cosine_similarity": cosine_similarity}

            except KeyError:
                result = {"id": user['id'], "twitter": 0}

            processed_user_count += 1
            save_results(results_id, result)
            print("finished in %0.3fs." % (time() - t0))
    return results


def build_variables_testing(usernames=None):
    """
    One of main function. Analyses and builds as much data as possible for a user
    :param usernames: List of user names to be analysed. If None, use all users from data
    """
    results = []
    users = get_users(usernames)
    # results = []
    for user in users:
        print("Processing and extracting variables for: {} ( {} ) ".format(user['name'], user['twitter_username']))
        t0 = time()

        slope = calc_slope(user)
        tweet_count = calc_tweet_count(user)
        # follower_count = calc_follower_number_last_week(user)

        weekly_count = calc_weekly_count_tweets(user)
        nOfTweetsLastWeek = weekly_count[-1]['count']
        mean, standard_deviation = calc_mean_and_SD(weekly_count)
        ratio = calc_ratio_of_weeks(weekly_count, 1, 4)
        difference = calc_diff_between_weeks(weekly_count, 1, 4)

        topicsFromTweets, topicsFromSurvey, cosine_similarity = congruence(user)

        result = {"twitter_username": user['twitter_username'],
                  "twitter_user_id": user['twitter_user_id'],
                  "name": user['name'],
                  "slope": slope,
                  "tweet_count": tweet_count,
                  # "follower_count": follower_count,
                  "mean": mean,
                  "standard_deviation": standard_deviation,
                  "ratio": ratio,
                  "difference": difference,
                  "tweets_last_week": nOfTweetsLastWeek,
                  "topicsFromTweets": topicsFromTweets,
                  "topicsFromSurvey": topicsFromSurvey,
                  "cosine_similarity": cosine_similarity}
        results.append(result)
        print("finished in %0.3fs." % (time() - t0))
    return results


def find_survey_topics():
    topics = []
    results = db[SURVEY_DATA].find()
    for result in results:
        topicsFromSurvey = ast.literal_eval(result['topn'])
        for topic in topicsFromSurvey:
            topics.append(topic[0])
    return list(set(topics))


def save_survey_topics(topics):
    for topic in topics:
        db[TOPIC_DICT].insert_one({"topic": topic, "keywords": []})
    return "Success"


def main():
    create_result = input(
        "Do you want to create new result collection in db?"
        "It will delete all previous records with type 'Variables' (y/n): ")
    if create_result == 'y':
        create_result = True
    else:
        create_result = False

    usernames = input(
        "If you want to run analysis for specific usernames, enter space seperated usernames. Otherwise, leave empty.").split()

    if not usernames:
        usernames = None

    build_variables(usernames, create_result)
    # pprint.pprint(build_variables_testing(usernames=['callumjodwyer']))


# pprint.pprint(build_variables_testing(usernames=['callumjodwyer']))
# pprint.pprint(build_variables_testing(usernames=['GutoAberconwy']))
# build_variables()
# print(save_survey_topics(find_survey_topics()))

main()
