from time import time
import sys
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, TfidfTransformer
from sklearn.decomposition import LatentDirichletAllocation, NMF
from pymongo import MongoClient
from sklearn.metrics import jaccard_similarity_score
from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.ensemble import VotingClassifier
from sklearn import svm
import numpy
from topicModel import print_top_words, nameTopics,calcAverageNumberOfKeywords

def classifyTweets(classifier='mnb'):
    client = MongoClient()
    db = client["sp"]
    datafile = open('topicSamples.dat', 'r')
    topic = ""
    testData = []
    testLabel = []
    for line in datafile:
        line = line.strip("\n")
        if topic == "":
            topic = line
        if line == "":
            topic = ""
        elif line == topic:
            pass
        else:
            testData.append(line)
            testLabel.append(topic)
    #
    # for data, topic in zip(testData, testLabel):
    #     print(data + " -> " + topic)

    target = []
    trainData = []
    uncategorized = 0
    categorized = 0
    correct = 0
    topics = list(db['datadict'].find())
    for topic in topics:
        target.append(topic["topic"])
        trainData.append(" ".join(list(topic["keywords"])))

    count_vect = CountVectorizer(stop_words="english")
    X_train_counts = count_vect.fit_transform(trainData)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)

    classifiers = {'svc': svm.SVC(probability=True),
                   'mnb': MultinomialNB(),
                   'vote': VotingClassifier(estimators=[('svc', svm.SVC(probability=True)), ('mnb', MultinomialNB(),)],
                                            voting='soft')}

    if classifier not in classifiers:
        print("Select correct classifier")
        exit()

    clf = classifiers[classifier]
    clf.fit(X_train_tfidf, target)

    X_new_counts = count_vect.transform(testData)
    X_new_tfidf = tfidf_transformer.transform(X_new_counts)

    predicted = clf.predict(X_new_tfidf)
    probabilities = clf.predict_proba(X_new_tfidf)

    for doc, category, realCat, probability in zip(testData, predicted, testLabel, probabilities):
        if numpy.unique(probability).size != 1:
            categorized += 1
            if category == realCat:
                correct += 1
            print('%r => %s (Real: %s) (%2.5f)' % (doc, category, realCat, numpy.amax(probability) * 100))
        else:
            uncategorized += 1

    print("Categorized tweets: %d " % categorized)
    print("Correctly categorized tweets: %d " % correct)
    print("%d %%" % ((correct * 1.0 / categorized) * 100))
    print("Uncategorized tweets: %d" % uncategorized)


def topicExtraction():
    client = MongoClient()
    db = client["sp"]
    datafile = open('topicSamples.dat', 'r')
    topic = ""
    testData = []
    testLabel = []
    for line in datafile:
        line = line.strip("\n")
        if topic == "":
            topic = line
        if line == "":
            topic = ""
        elif line == topic:
            pass
        else:
            testData.append(line)
            testLabel.append(topic)

    tfidf_vectorizer = TfidfVectorizer(stop_words='english')
    t0 = time()
    tfidf = tfidf_vectorizer.fit_transform(testData)
    print("...done in %0.3fs." % (time() - t0))

    # Extract topics
    print("Fitting the NMF model")
    t0 = time()
    nmf = NMF(n_components=6, random_state=1,
              alpha=.1, l1_ratio=.5).fit(tfidf)
    print("...done in %0.3fs." % (time() - t0))

    # Print results
    print("\nTopics in LDA model:")
    tf_feature_names = tfidf_vectorizer.get_feature_names()
    # Display keywords
    print_top_words(nmf, tf_feature_names, 100)

    # Name topics resulted from extraction
    nameTopics(nmf, tf_feature_names)


if __name__ == "__main__":
    # classifyTweets(sys.argv[1])
    calcAverageNumberOfKeywords()
    topicExtraction()