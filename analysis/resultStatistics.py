from pymongo import MongoClient
from bson import ObjectId
import sys
import pprint
import json
import os

sys.path.insert(0, '..')
from config import mongoDbURI, topicModelling, mongodbName

# Configure database client
client = MongoClient(host=mongoDbURI())
db = client[mongodbName]


def calcStatisticsForUserGroupTweets(result_id):
    data = {}
    documentID = ObjectId(result_id)
    resultDoc = db['results'].find_one({"_id": documentID})
    for result in resultDoc['results']:
        if result['party'] not in data:
            data[result['party']] = {}

        if result['category'] in data[result['party']]:
            data[result['party']][result['category']] += 1
        else:
            data[result['party']][result['category']] = 1
    f = open(os.path.join("results", result_id + ".json"), "w")
    json.dump(data, f)
    f.close()
    pp = pprint.PrettyPrinter()
    pp.pprint(data)


def calcStatisticsForPartyGroupTweets(result_id):
    data = {}
    documentID = ObjectId(result_id)
    resultDoc = db['results'].find_one({"_id": documentID})
    for result in resultDoc['results']:
        if result['party_name'] not in data:
            data[result['party_name']] = {}

        if result['category'] in data[result['party_name']]:
            data[result['party_name']][result['category']] += 1
        else:
            data[result['party_name']][result['category']] = 1

    f = open(os.path.join("results", result_id + ".json"), "w")
    json.dump(data, f)
    f.close()
    pp = pprint.PrettyPrinter()
    pp.pprint(data)
