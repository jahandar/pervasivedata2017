__author__ = 'Jahandar Musayev'

from time import time
import sys
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, TfidfTransformer
from sklearn.decomposition import LatentDirichletAllocation, NMF
from pymongo import MongoClient
from sklearn.metrics import jaccard_similarity_score
from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.ensemble import VotingClassifier
from sklearn import svm
import numpy
from nltk import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.tag import pos_tag
import re
import emoji

sys.path.insert(0, '..')
from config import mongoDbURI, topicModelling, mongodbName

TOPIC_DICT = topicModelling["topic_dict"]


class LemmaTokenizer(object):
    def __init__(self):
        self.wnl = WordNetLemmatizer()

    def __call__(self, doc):

        tokens = []
        for t in pos_tag(word_tokenize(doc)):
            if self.checkConditions(t):
                decoded = t[0]
                allchars = [str for str in decoded]
                token = ''.join([c for c in allchars if c not in emoji.UNICODE_EMOJI])
                if token:
                    tokens.append(token)

        return tokens

    def checkConditions(self, t):
        filter_words = ['http', 'https', '@', 'rt']
        http_pattern = "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
        url_pattern = "//(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
        profile_pattern = "[a-zA-Z]*_[a-zA-Z]*"
        number_pattern = "[+|-]*[0-9]*[+|-]*"
        if len(t[0]) < 3: return False
        if t[1][0] != "N": return False
        if re.match(http_pattern, t[0]): return False
        if re.match(url_pattern, t[0]): return False
        if re.match(profile_pattern, t[0]): return False
        # if re.match(number_pattern, t[0]): return False
        if t[0] in filter_words: return False
        return True


class Analyser:
    """
    Module for analysing tweets.
    Includes topic modelling by LDA and NMF, naming topics using classification.
    """

    def __init__(self, verbose=0):
        client = MongoClient(host=mongoDbURI())
        self.db = client[mongodbName]
        self.nTopWords = topicModelling["nTopWords"]
        if self.nTopWords == 0:
            self.nTopWords = self.calcAverageNumberOfKeywords()
        self.classifier = topicModelling["classifier"]
        self.verbose = verbose

    def LDA(self, data, nTopics):
        if self.verbose: print("Preparing tf matrix")
        count_vector = CountVectorizer(max_df=0.9, stop_words="english", tokenizer=LemmaTokenizer())
        t0 = time()
        try:
            tf = count_vector.fit_transform(data)
            tf_feature_names = count_vector.get_feature_names()
        except ValueError as value_error:
            print("An error occurred ")
            return -1, -1

        if self.verbose: print("...done in %0.3fs." % (time() - t0))

        if self.verbose: print("Fitting LDA models")
        lda = LatentDirichletAllocation(n_components=nTopics, max_iter=5,
                                        learning_method='online',
                                        learning_offset=10.,
                                        random_state=0)
        t0 = time()
        lda.fit(tf)

        if self.verbose: print("...done in %0.3fs." % (time() - t0))
        return lda, tf_feature_names

    def NMF(self, data, nTopics):
        if self.verbose: print("Preparing tf-idf matrix")
        tfidf_vectorizer = TfidfVectorizer(max_df=0.8, min_df=2,
                                           stop_words='english')
        t0 = time()
        tfidf = tfidf_vectorizer.fit_transform(data)
        tfidf_feature_names = tfidf_vectorizer.get_feature_names()

        print("...done in %0.3fs." % (time() - t0))

        # Extract topics
        print("Fitting the NMF model")
        t0 = time()
        nmf = NMF(n_components=nTopics, random_state=1,
                  alpha=.1, l1_ratio=.5).fit(tfidf)
        print("...done in %0.3fs." % (time() - t0))

        return nmf, tfidf_feature_names

    def calcAverageNumberOfKeywords(self):
        topics = list(self.db['datadict'].find())
        # Prepare training data using keywords
        nKeywords = 0
        nTotalTopics = 0
        for topic in topics:
            nTotalTopics += 1
            nKeywords += len(list(topic["keywords"]))

        return int(nKeywords / nTotalTopics)

    def print_top_words(self, model, feature_names, n_top_words):
        for topic_idx, topic in enumerate(model.components_):
            print("Topic #%d:" % topic_idx)
            print(" ".join([feature_names[i]
                            for i in topic.argsort()[:-n_top_words - 1:-1]]))

    def printTopics(self, topic_ids, predicted, probabilities):
        for topic_id, category, probability in zip(topic_ids, predicted, probabilities):
            if numpy.unique(probability).size != 1:
                print('Topic %r => %s (%2.5f)' % (topic_id, category, numpy.amax(probability) * 100))

    def classification(self, trainData, trainLabels, realData, classifier):
        """
        Classification using given training and real data.
        Performs feature extraction from text data using tf-idf (term frequency–inverse document frequency).
        Types of classifier:
            SVC - Support Vector Classifier
            MNB - Multinomial Naive-Bayes Classifier
            Vote - Uses all classifiers and performs voting to predict the class
        :param trainData: 1-D array of training text samples
        :param trainLabels: 1-D array of labels of text samples
        :param realData: 1-D array of data to be classified
        :param classifier: classifier to be used
        :return:    Predicted class and calculated probabilities for each class
        """

        # Set up count vectorizer and transformer to tf-idf.
        # Removes stop words for english language
        count_vectorizer = CountVectorizer(max_df=0.95, stop_words="english", tokenizer=LemmaTokenizer())
        tfidf_transformer = TfidfTransformer()

        # Use count vectorizer and frequency tranformer to for feature extraction
        # For training data
        train_counts = count_vectorizer.fit_transform(trainData)
        train_tfidf = tfidf_transformer.fit_transform(train_counts)

        # For classification data
        data_counts = count_vectorizer.transform(realData)
        data_tfidf = tfidf_transformer.transform(data_counts)

        # List of classifiers and their setups
        classifiers = {'svc': svm.SVC(probability=True),
                       'mnb': MultinomialNB(),
                       'vote': VotingClassifier(
                           estimators=[('svc', svm.SVC(probability=True)), ('mnb', MultinomialNB(),)],
                           voting='soft')}

        # Select classifier
        if classifier not in classifiers:
            print("Select correct classifier")
            exit()
        clf = classifiers[classifier]

        # Train classifier using training data
        clf.fit(train_counts, trainLabels)

        # Predict classes for real data
        predicted = clf.predict(data_counts)
        probabilities = clf.predict_proba(data_counts)

        # Return class and probabilities of all classes
        return predicted, probabilities

    def nameTopics(self, model, feature_names):
        """
        Naming extracted topics using keywords of data dictionary in database
        :param model: Model to classify
        :param feature_names:
        :return:
        """

        # Fetch topics from database
        topics = list(self.db[TOPIC_DICT].find())

        # Prepare training data using keywords
        target = []
        trainData = []
        totalKeywords = 0
        for topic in topics:
            target.append(topic["topic"])
            trainData.append(" ".join(list(topic["keywords"])))
            totalKeywords += len(list(topic["keywords"]))

        # Process model for classification
        datatoclassify = []
        topic_ids = []
        for topic_id, topicm in enumerate(model.components_):
            datatoclassify.append(" ".join([feature_names[i] for i in topicm.argsort()[:-self.nTopWords - 1:-1]]))
            topic_ids.append(topic_id)

        predicted, probabilities = self.classification(trainData, target, datatoclassify, self.classifier)

        return topic_ids, predicted, probabilities

    def saveResults(self, results, type, info=""):

        return self.db["results"].insert_one({"type": type, "results": results, "information": info}).inserted_id
        # self.db["results"].update_one({"_id": resultID}, {"$push": {"results": result}})
