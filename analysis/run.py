"""
    Default script file for running analysis of tweets
"""

from analyseTweets import useLDA as LDA, useNMF as NMF, useLDAwithUsernames as LDAwUsernames
from analyseTweets import classifyTweets, classifyTweetsByUsername, classifyTweetsByParty
from resultStatistics import calcStatisticsForUserGroupTweets, calcStatisticsForPartyGroupTweets

"""Uncomment any of lines below to user methods"""

"""Extract topic by using one of two method"""
# LDA()
# NMF()
LDAwUsernames(usernames=['callumjodwyer'])

"""Classify each tweet (because number of tweets is very large, it will take a lot time)"""
"""If first argument is omitted, Multinomial Naive-Bayes Classifier will be user as default"""
# classifyTweets()

"""Classify tweets by grouping them with a criteria"""
"""If first argument is omitted, Multinomial Naive-Bayes Classifier will be user as default"""
"""If second argument is omitted, method will use all possible values(for ex: all candidates, all parties)"""
# classifyTweetsByUsername(usernames=["1tomcorbin"])
# classifyTweetsByParty(partyIDs=["party:52"])

# calcStatisticsForUserGroupTweets("597e3a75b7c6667ab9ae456b")
# calcStatisticsForPartyGroupTweets("59812af5b7c66655d6b4bb77")