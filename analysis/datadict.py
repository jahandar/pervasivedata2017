"""

Python script to manipulate keywords in database (insertion, extension)
->  Inserts keywords from csv file to mongodb database.
    First column of each row is identified as name of the topic.
    Rest are keywords.
    Each topic can occupy more than 1 row.

->  Extends dictionary by finding words related to topics or keywords.
    Uses http://conceptnet.io/ or https://www.datamuse.com/ .
    You can select which website to use.

    Finds word by two method:
    1. words related to topic names in database
    2. words related to topic names and keywords on that topic in database.
    In case of 2nd method, it will use first 10 words, otherwise, there will be
    a lot new words which can be not related.
    You can select which method to use.
"""

import requests, csv, sys
from pymongo import MongoClient

sys.path.insert(0, '..')
from config import mongoDbURI, topicModelling, mongodbName

TOPICS_DICT = topicModelling["topic_dict"]

def insertDatadict(path):
    """
    Inserts data into mongodb
    :param path:
    :return: nothing
    """
    dataFile = open(path, newline='')
    rows = csv.reader(dataFile)
    for row in rows:
        row = list(filter(None, row))
        coll.update_one({"topic": row[0]}, {"$push": {"keywords": {"$each": row[1:]}}}, upsert=True)
    print("Data imported successfully")


def useConceptNet(keywordRel=False):
    """Uses ConceptNet to find related words

    :param keywordRel: If set, uses keywords as well as topics to find related words
    :return nothing

    """

    # For each topic, fetch related words
    newKeywordsCount = 0
    for dbtopic in topics:

        # Clear name of topic
        topic = "_".join(dbtopic["topic"].strip(" ").split(" ")).lower()
        bagofwords = []

        # Send request to get related words
        url = "http://api.conceptnet.io"
        obj = requests.get(url + '/related/c/en/' + topic + '?filter=/c/en').json()
        relatedWords = obj["related"]

        # Decode and get new words
        for ref in relatedWords:
            word = " ".join(ref["@id"].split("/")[-1].split("_"))
            bagofwords.append(word)

        # If keywordRel is set, do the same as belov for keywords
        if keywordRel:
            keywords = dbtopic["keywords"]
            for dbkeyword in keywords[:10]:
                keyword = "_".join(dbkeyword.strip(" ").split(" ")).lower()
                kwRelWords = requests.get(url + '/related/c/en/' + keyword + '?filter=/c/en').json()
                for node in kwRelWords["related"]:
                    word = " ".join(node["@id"].split("/")[-1].split("_"))
                    bagofwords.append(word)

        # Update database
        coll.update_one({"_id": dbtopic["_id"]}, {"$addToSet": {"keywords": {"$each": bagofwords}}}, )

        # Print statistics
        print("Topic: {0} - {1} new words".format(topic, len(bagofwords)))
        newKeywordsCount += len(bagofwords)
    print("Dictionary extended successfully. %d new keywords inserted. " % newKeywordsCount)


def useDatamuse(keywordRel=False):
    """Uses useDatamuse to find related words

        :param keywordRel: If set, uses keywords as well as topics to find related words
        :return nothing

    """

    # For each topic, fetch related words
    newKeywordsCount = 0
    for dbtopic in topics:

        # Clear name of topic
        topic = "+".join(dbtopic["topic"].strip(" ").split(" ")).lower()
        bagofwords = []

        # Send request to get related words
        url = "https://api.datamuse.com"
        topicRelWords = requests.get(url + '/words?rel_trg=' + topic).json()

        # Decode and get new words
        for node in topicRelWords:
            word = node["word"]
            bagofwords.append(word)

        # If keywordRel is set, do the same as belov for keywords
        if keywordRel:
            keywords = dbtopic["keywords"]
            for dbkeyword in keywords[:10]:
                keyword = "+".join(dbkeyword.strip(" ").split(" ")).lower()
                kwRelWords = requests.get(url + '/words?rel_trg=' + keyword).json()
                for node in kwRelWords[:10]:
                    word = node["word"]
                    bagofwords.append(word)

        # Update database
        coll.update_one({"_id": dbtopic["_id"]}, {"$addToSet": {"keywords": {"$each": bagofwords}}}, )

        # Print statistics
        print("Topic: {0} - {1} new words".format(topic, len(bagofwords)))
        newKeywordsCount += len(bagofwords)
    print("Dictionary extended successfully. %d new keywords inserted. " % newKeywordsCount)


if __name__ == "__main__":

    #    Main body of script

    print("This script is intended to import and/or extend data dictionary. \n"
          "For extension to work properly, data must be imported first")
    action = ""
    while True:
        action = input(
            "Please enter 'import' for data import to mongodb, 'extend' to extend data dictionary. Enter 'quit' to exit: ")
        if action == "quit":
            exit()
        if action == "import" or action == "extend":
            break

    client = MongoClient()
    db = client['sp']
    coll = db[TOPICS_DICT]
    topics = list(coll.find())

    if action == "import":
        path = input("Please enter path to file: ")
        insertDatadict(path)

    if action == "extend":
        keywordRel = input(
            "Do you want to use keywords too to extend data dictionary? Otherwise, only topics will be used. (yes/no): ")
        if keywordRel == 'yes':
            keywordRel = True
        else:
            keywordRel = False

        website = ""
        while True:
            website = input(
                "Please enter 'cn' to extend using website 'ConceptNet', 'dm' to extend using 'Datamuse' (not much difference tho): ")
            if website == "cn" or website == "dm":
                break

        if website == "cn":
            useConceptNet(keywordRel=keywordRel)

        if website == 'dm':
            useDatamuse(keywordRel=keywordRel)
