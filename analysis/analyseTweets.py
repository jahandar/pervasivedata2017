"""
    Script for data analysis
    > Classify tweets according to topics
    > Group and classify groups according to topics
    > Cluster tweets into different topics
    > Label clusters
"""

from time import time
import sys

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, TfidfTransformer
from sklearn.decomposition import LatentDirichletAllocation, NMF
from pymongo import MongoClient
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import VotingClassifier
from sklearn import svm
import numpy

sys.path.insert(0, '..')
from config import mongoDbURI, topicModelling, mongodbName

# Configure database client
client = MongoClient(host=mongoDbURI())
db = client[mongodbName]

# Import configurations
nFeatures = topicModelling["nFeatures"]
if nFeatures == 0:
    nFeatures = None
nSamples = topicModelling["nSamples"]
nTopWords = topicModelling["nTopWords"]
nTopics = topicModelling["nTopics"]
displayKeywords = topicModelling["displayKeywords"]
classifyTopics = topicModelling["classifyTopics"]
classifier = topicModelling["classifier"]
tweetsCollection = topicModelling["tweetsCollection"]
candidatesCollection = topicModelling["candidatesCollection"]
dateRange = topicModelling["dateRange"]
startDate = topicModelling["startDate"]
endDate = topicModelling["endDate"]
saveResults = topicModelling["saveResults"]

# Pipelines to fetch data from database
# For topic extraction
pipelineForTopicExtraction = [{'$sample': {'size': nSamples}}, {'$project': {'tweet_text': 1, '_id': 0}}]


# For tweet classification
# TODO

def calcAverageNumberOfKeywords():
    topics = list(db['datadict'].find())

    # Prepare training data using keywords
    nKeywords = 0
    nTotalTopics = 0
    for topic in topics:
        nTotalTopics += 1
        nKeywords += len(list(topic["keywords"]))
    global nTopWords
    if nTopWords == 0:
        nTopWords = int(nKeywords / nTotalTopics)


def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic #%d:" % topic_idx)
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
    print()


def useLDA():
    """
    Topic extraction using Latent Drichlet Allocation
    :return: none
    """
    print("Using Latent Dirichlet Allocation for clustering")
    print("Loading dataset")

    # Database operations
    # Fetch samples

    samples = list(db[tweetsCollection].aggregate(pipelineForTopicExtraction))

    if dateRange:
        samples = list(db['tweets'].find({'tweet_created_at': {'$gte': startDate,
                                                               '$lt': endDate}}))
    else:
        samples = list(db['tweets'].find())

    # Decide number of topics
    global nTopics
    if nTopics == 0:
        nTopics = db['datadict'].find().count()
    print("%s topics are discovered" % nTopics)

    # Prepare data
    dataset = []
    for data in samples:
        dataset.append(data["tweet_text"])

    print(dataset)

    # Process data using count vectorizer
    count_vector = CountVectorizer(max_df=0.8, min_df=2, max_features=nFeatures, stop_words="english")
    t0 = time()
    tf = count_vector.fit_transform(dataset)
    print("...done in %0.3fs." % (time() - t0))

    # Extract topics
    print("Fitting LDA models")
    lda = LatentDirichletAllocation(n_components=nTopics, max_iter=5,
                                    learning_method='online',
                                    learning_offset=10.,
                                    random_state=0)
    t0 = time()
    results = lda.fit(tf)
    print("...done in %0.3fs." % (time() - t0))

    print(results)
    exit()

    # Print results
    print("\nTopics in LDA model:")
    tf_feature_names = count_vector.get_feature_names()
    # Display keywords
    if displayKeywords:
        print_top_words(results, tf_feature_names, nTopWords)

    # Name topics resulted from extraction
    if classifyTopics:
        nameTopics(lda, tf_feature_names)


def useNMF():
    """
    Topic extraction using Non-negative Matrix Factorization
    :return: none
    """
    print("Using Non-negative Matrix Factorization for clustering")
    print("Loading dataset")

    # Database operations
    # Fetch samples
    samples = list(db[tweetsCollection].aggregate(pipelineForTopicExtraction))

    if dateRange:
        samples = list(db['tweets'].find({'tweet_created_at': {'$gte': startDate,
                                                               '$lt': endDate}}))
    else:
        samples = list(db['tweets'].find())

    # Decide number of topics
    global nTopics
    if nTopics == 0:
        nTopics = db['datadict'].find().count()
    print("%s topics are discovered" % nTopics)

    # Prepare data
    dataset = []
    for data in samples:
        dataset.append(data["tweet_text"])

    print("Extracting tf-idf features for NMF")
    # Process data using tf-idf vectorizer
    tfidf_vectorizer = TfidfVectorizer(max_df=0.8, min_df=10,
                                       stop_words='english')
    t0 = time()
    tfidf = tfidf_vectorizer.fit_transform(dataset)
    print("...done in %0.3fs." % (time() - t0))

    # Extract topics
    print("Fitting the NMF model")
    t0 = time()
    nmf = NMF(n_components=nTopics, random_state=1,
              alpha=.1, l1_ratio=.5).fit(tfidf)
    print("...done in %0.3fs." % (time() - t0))

    # Print results
    print("\nTopics in NMF model:")
    tfidf_feature_names = tfidf_vectorizer.get_feature_names()
    # Display keywords
    if displayKeywords:
        print_top_words(nmf, tfidf_feature_names, nTopWords)

    # Name topics resulted from extraction
    if classifyTopics:
        nameTopics(nmf, tfidf_feature_names)


def nameTopics(model, feature_names, printResults=True):
    """
    Naming extracted topics using keywords of data dictionary in database
    :param model: Model to classify
    :param feature_names:
    :return:
    """

    # Fetch topics from database
    topics = list(db['datadict'].find())

    # Prepare training data using keywords
    target = []
    trainData = []
    totalKeywords = 0
    for topic in topics:
        target.append(topic["topic"])
        trainData.append(" ".join(list(topic["keywords"])))
        totalKeywords += len(list(topic["keywords"]))


    # Process model for classification
    datatoclassify = []
    topic_ids = []
    for topic_id, topicm in enumerate(model.components_):
        datatoclassify.append(" ".join([feature_names[i] for i in topicm.argsort()[:-nTopWords - 1:-1]]))
        topic_ids.append(topic_id)

    predicted, probabilities = classification(trainData, target, datatoclassify)

    # Display results
    if printResults:
        printTopics(topic_ids, predicted, probabilities)

    return topic_ids, predicted, probabilities


def printTopics(topic_ids, predicted, probabilities):
    for topic_id, category, probability in zip(topic_ids, predicted, probabilities):
        if numpy.unique(probability).size != 1:
            print('Topic %r => %s (%2.5f)' % (topic_id, category, numpy.amax(probability) * 100))


def classifyTweets():
    """
    Classifies each tweet using keywords form database.
    Mostly is data preparation
    :return: none
    """

    print("Classifying tweets")
    t0 = time()
    # Keep statistical information
    classifiedCount = 0
    notClassifiedCount = 0

    # Variables to hold data
    target = []
    trainData = []
    datatoclassify = []

    print("Fetching data form database and processing")
    # Database operations
    topics = list(db['datadict'].find())
    # samples = list(db[tweetsCollection].aggregate(pipelineForTopicExtraction))
    samples = list(db[tweetsCollection].find({}, {'tweet_text': 1, '_id': 0}).limit(100))

    # Prepare training data
    # For each topic, concatenate keywords (they will be processed on classification)
    for topic in topics:
        target.append(topic["topic"])
        trainData.append(" ".join(list(topic["keywords"])))

    # Prepare classification data
    for sample in samples:
        datatoclassify.append(sample['tweet_text'])

    print("...done in %0.3fs." % (time() - t0))
    print("Starting classifying tweets")

    # Classification
    prediction, probabilities = classification(trainData, target, datatoclassify)
    print("...done in %0.3fs." % (time() - t0))

    # Display results
    for doc, category, probability in zip(datatoclassify, prediction, probabilities):
        # If probabilities of all classes are equal, classification is not successful
        if numpy.unique(probability).size != 1:
            classifiedCount += 1
            print('%r => %s (%2.5f)' % (doc, category, numpy.amax(probability) * 100))
        else:
            print('%r => %s' % (doc, " not categorized"))
            notClassifiedCount += 1

    print("Categorized tweets: %d " % classifiedCount)
    print("Uncategorized tweets: %d" % notClassifiedCount)


def classifyTweetsByUsername(usernames=None):
    """
    Groups tweets by user name and classifies
    :param usernames: Array of username strings classify. If None, will use all usernames in database
    :return: none
    """

    resultID = None

    if saveResults:
        resultID = db["results"].insert_one({"type": "GroupByCandidate", "results": []}).inserted_id

    print("Classifying tweets by grouping them")
    t0 = time()
    # Keep statistical information
    classifiedCount = 0
    notClassifiedCount = 0

    # Variables to hold data
    target = []
    trainData = []

    print("Fetching data form database and processing")
    # Database operations
    # Get screen name on tweets from database
    topics = list(db['datadict'].find())
    if usernames:
        users = list(db[candidatesCollection].find({"twitter_username": usernames},
                                                   {"twitter_user_id": 1, "twitter_username": 1,
                                                    "party_name": 1, "party_id": 1}))
    else:
        users = list(db[candidatesCollection].find({"twitter_username": {"$exists": True}},
                                                   {"twitter_user_id": 1, "twitter_username": 1,
                                                    "party_name": 1, "party_id": 1}))

    # For each user, select tweets that belong to that user
    # Concatenate them all together to create sample (they will be processed on classification) for each user
    print("...done in %0.3fs." % (time() - t0))
    print("Starting classifying tweets")
    for user in users:
        datatoclassify = []
        if dateRange:
            allTweets = ".".join(
                list(db[tweetsCollection].distinct("tweet_text", {'twitter_user_id': user['twitter_user_id'],
                                                                  'tweet_created_at': {'$gte': startDate,
                                                                                       '$lt': endDate}})))
        else:
            allTweets = ".".join(
                list(db[tweetsCollection].distinct("tweet_text", {'twitter_user_id': user['twitter_user_id']})))
        datatoclassify.append(allTweets)

        # Prepare training data
        # For each topic, concatenate keywords (they will be processed on classification)
        for topic in topics:
            target.append(topic["topic"])
            trainData.append(" ".join(list(topic["keywords"])))

        # Classification
        prediction, probabilities = classification(trainData, target, datatoclassify)

        # Display results
        for category, probability in zip(prediction, probabilities):
            # If probabilities of all classes are equal, classification is not successful
            if numpy.unique(probability).size != 1:
                classifiedCount += 1
                print('%r => %s (%2.5f)' % (user["twitter_username"], category, numpy.amax(probability) * 100))
                if saveResults:
                    result = {"user_name": user["twitter_username"],
                              "party": user["party_name"],
                              "party_id": user["party_id"],
                              "category": category,
                              "probability": (numpy.amax(probability) * 100)}
                    db["results"].update_one({"_id": resultID}, {"$push": {"results": result}})
            else:
                print('%r => %s' % (user["twitter_username"], " not categorized"))
                if saveResults:
                    result = {"user_name": user["twitter_username"],
                              "party": user["party_name"],
                              "party_id": user["party_id"],
                              "category": "not categorized"}
                    db["results"].update_one({"_id": resultID}, {"$push": {"results": result}})
                notClassifiedCount += 1
    print("...done in %0.3fs." % (time() - t0))
    print("Categorized groups: %d " % classifiedCount)
    print("Uncategorized groups: %d" % notClassifiedCount)


def classifyTweetsByParty(partyIDs=None):
    """
    Groups tweets by party and classifies
    :param partyIDs: Array of party id strings in form "party:<id>" to classify. If None, will use all parties in database
    :return: none
    """

    resultID = None

    if saveResults:
        resultID = db["results"].insert_one({"type": "GroupsByParty", "results": []}).inserted_id

    print("Classifying tweets by grouping them")
    t0 = time()
    # Keep statistical information
    classifiedCount = 0
    notClassifiedCount = 0

    # Variables to hold data
    target = []
    trainData = []

    print("Fetching data form database and processing")
    # Database operations
    # Get screen name on tweets from database
    topics = list(db['datadict'].find())

    # For each user, select tweets that belong to that user
    # Concatenate them all together to create sample (they will be processed on classification) for each user
    print("...done in %0.3fs." % (time() - t0))
    print("Starting classifying tweets")
    for party in partyIDs:
        print(party)
        datatoclassify = []
        allTweets = ""
        usernames = list(db[candidatesCollection].find({"party_id": party, "twitter_user_id": {"$exists": True}},
                                                       {"party_id": 1, "party_name": 1, "twitter_user_id": 1,
                                                        "twitter_username": 1}).limit(20))

        for user in usernames:
            if dateRange:
                allTweets += ".".join(
                    list(db[tweetsCollection].distinct("tweet_text", {'twitter_user_id': user['twitter_user_id'],
                                                                      'tweet_created_at': {'$gte': startDate,
                                                                                           '$lt': endDate}})))
            else:
                allTweets += ".".join(
                    list(db[tweetsCollection].distinct("tweet_text", {'twitter_user_id': user['twitter_user_id']})))
        datatoclassify.append(allTweets)

        # Prepare training data
        # For each topic, concatenate keywords (they will be processed on classification)
        for topic in topics:
            target.append(topic["topic"])
            trainData.append(" ".join(list(topic["keywords"])))

        # Classification
        prediction, probabilities = classification(trainData, target, datatoclassify)

        # Display results
        for category, probability in zip(prediction, probabilities):
            # If probabilities of all classes are equal, classification is not successful
            if numpy.unique(probability).size != 1:
                classifiedCount += 1
                print('%r => %s (%2.5f)' % (usernames[0]["party_name"], category, numpy.amax(probability) * 100))
                if saveResults:
                    result = {"party_name": usernames[0]["party_name"],
                              "category": category,
                              "probability": (numpy.amax(probability) * 100)}
                    db["results"].update_one({"_id": resultID}, {"$push": {"results": result}})
            else:
                print('%r => %s' % (usernames[0]["party_name"], " not categorized"))
                if saveResults:
                    result = {"party_name": usernames[0]["party_name"],
                              "category": "not categorized"}
                    db["results"].update_one({"_id": resultID}, {"$push": {"results": result}})
                notClassifiedCount += 1
    print("...done in %0.3fs." % (time() - t0))
    print("Categorized groups: %d " % classifiedCount)
    print("Uncategorized groups: %d" % notClassifiedCount)


def classification(trainData, trainLabels, realData):
    """
    Classification using given training and real data.
    Performs feature extraction from text data using tf-idf (term frequency–inverse document frequency).
    Types of classifier:
        SVC - Support Vector Classifier
        MNB - Multinomial Naive-Bayes Classifier
        Vote - Uses all classifiers and performs voting to predict the class
    :param trainData: 1-D array of training text samples
    :param trainLabels: 1-D array of labels of text samples
    :param realData: 1-D array of data to be classified
    :return:    Predicted class and calculated probabilities for each class
    """

    # Set up count vectorizer and transformer to tf-idf.
    # Removes stop words for english language and words occur more than 80% of the text
    count_vectorizer = CountVectorizer(max_df=0.95, min_df=2, stop_words="english")
    tfidf_transformer = TfidfTransformer()

    # Use count vectorizer and frequency tranformer to for feature extraction
    # For training data
    train_counts = count_vectorizer.fit_transform(trainData)
    train_tfidf = tfidf_transformer.fit_transform(train_counts)

    # For classification data
    data_counts = count_vectorizer.transform(realData)
    data_tfidf = tfidf_transformer.transform(data_counts)

    # List of classifiers and their setups
    classifiers = {'svc': svm.SVC(probability=True),
                   'mnb': MultinomialNB(),
                   'vote': VotingClassifier(estimators=[('svc', svm.SVC(probability=True)), ('mnb', MultinomialNB(),)],
                                            voting='soft')}

    # Select classifier
    if classifier not in classifiers:
        print("Select correct classifier")
        exit()
    clf = classifiers[classifier]

    # Train classifier using training data
    clf.fit(train_tfidf, trainLabels)

    # Predict classes for real data
    predicted = clf.predict(data_tfidf)
    probabilities = clf.predict_proba(data_tfidf)

    # Return class and probabilities of all classes
    return predicted, probabilities


def main():
    """ Main body of the script (DEPRECATED)

            Input topicModel.py <operation>
            -----
            lda : Extract topics using Latent Dirichlet Allocation
            nmf : Extract topics using Non-negative Matrix Factorization

            Input topicModel.py <operation> <type>
            -----
            class: Classify tweets to topics in database
                mnb : Classify using Multinomial Naive-Bayes
                svc: Classify using Support Vector Classifier
                vote: Ensemble classifiers above and use soft voting
    """

    calcAverageNumberOfKeywords()

    if len(sys.argv) != 3:
        print("Please use correct input")
        exit()

    # Topic extraction
    if sys.argv[1].lower() == "extract":
        # Implement Latent Dirichlet Allocation (recommended)
        if sys.argv[2].lower() == "lda":
            useLDA()

        # Implements Non-negative Matrix Factorization
        elif sys.argv[2].lower() == "nmf":
            useNMF()

    elif sys.argv[1].lower() == "class":
        classifyTweets(sys.argv[2].lower())
    elif sys.argv[1].lower() == "group":
        classifyTweetsByUsername(sys.argv[2].lower())
    else:
        print("Please select correct method (lda or nmf")


if __name__ == "__main__":
    main()
    client.close()
