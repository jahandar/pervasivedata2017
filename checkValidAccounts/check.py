#!/usr/bin/python3
import csv
import json
import time
import sys
import requests


def main():
    # opening file using utf 8 encoding, problems occur otherwise
    csvfile = open('datafiles/candidates.csv', newline='', encoding='utf-8')
    candidatesCsv = csv.reader(csvfile)
    # initialising a counter
    i = 0
    # initialising the data output dictionary
    data = {}
    # initialising a flag for start point
    start = True
    for row in candidatesCsv:
        # incrementing counter and printing progress
        i += 1
        print("Candidate " + str(i))
        twitterID = row[27]
        if bool(twitterID) and twitterID.isdigit():
            if start:
                # starting right after this id
                if twitterID == "502115759":
                    start = False
            else:
                # waiting a second before each request to avoid unpleasantness
                time.sleep(1)
                # creating url for the id
                url = "https://twitter.com/intent/user?user_id=" + twitterID
                # getting response for the url
                response = requests.get(url)
                # if there is a page user exists, else it doesn't
                # 1 means exists, 2 means not
                if response.status_code == 200:
                    data[twitterID] = 1
                else:
                    data[twitterID] = 2
        # output the dictionary every 500 steps
        if i % 500 == 0:
            print("MILESTONE REACHED: " + str(i))
            dataJson = open('TwitterID.json', 'a+')
            json.dump(data, dataJson)
            data = {}


if __name__ == '__main__':
    main()
